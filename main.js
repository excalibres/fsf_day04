/**
 * Created by jk_87 on 31/3/2016.
 */
// Load express that we installed previously
var express = require("express");
// Create an app
var app = express();

// Define my document root
// Static resources will be served from here
app.use(
    express.static(__dirname + "/public")
);
//Start server to listen on port 3000
app.listen(2000, function () {
        console.info("Application is listening on port 2000")
    });